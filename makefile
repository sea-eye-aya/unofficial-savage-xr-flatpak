# Makefile for:
# Unofficial Flatpak Bootstrap (installer/update/runner) for
# Newerth's Savage XR "Battle for Newerth"
# By C.I.A.
#

#
# -----------------------------------------------------------------------------
# Local Flatpak Repository to place the built Flatpak in.
#
localFlatpakRepo = my-flatpak-repo

#
# -----------------------------------------------------------------------------
# Local directory used to build the flatpak in prior to importing into 
# local Flatpak repository.
#
localFlatpakDir = savagexr

#
# -----------------------------------------------------------------------------
# The official Flathub repository URL (can't see this ever changing, but...)
#
externalFlathubRepo = https://flathub.org/repo/flathub.flatpakrepo

# ----------------------------------------------------------------------------
# Get the version number.
# Makefile expands $ so escape with $$
#
versionNumber := ` grep -o '^VERSION="\([\d]*.[\d]*.[\d]*\).*$$' bootstrapsavagexr.sh | sed -e 's/^VERSION="//' -e 's/"$$//'`

#
# -----------------------------------------------------------------------------
# target: all - default Make target, build all.
#
.PHONY: all
all: banner remoteFlathub installRuntimeSDK buildFlatpak

#
# -----------------------------------------------------------------------------
# target: help - display list of available targets.
#
.PHONY: help
help: banner
	@echo "Available targets:"
	@echo ""
	@grep "^# target:" [Mm]akefile
	@echo ""

#
# -----------------------------------------------------------------------------
# target: banner - display a description of makefile.
#
.PHONY: banner
banner:
	@echo ""
	@echo "Unofficial Flatpak Bootstrap (installer/update/runner) for"
	@echo "Newerth's Savage XR 'Battle for Newerth'"
	@echo "By C.I.A."
	@echo "	                      Version: '$(versionNumber)'"
	@echo ""
	@echo "	Local Flatpak repo           : '$(localFlatpakRepo)'"
	@echo "	Local Flatpak build directory: '$(localFlatpakDir)'"
	@echo "	External Flathub repo        : '$(externalFlathubRepo)'"
	@echo ""

#
# -----------------------------------------------------------------------------
# target: remoteFlathub - Add the remote Flathub repository (if not already added) to users' Flatpak remote list.
#
.PHONY: remoteFlathub
remoteFlathub:
	flatpak --user remote-add --if-not-exists flathub $(externalFlathubRepo)

#
# -----------------------------------------------------------------------------
# target: installRuntimeSDK - Install the Freedesktop Runtime Platform and SDK.
#
.PHONY: installRuntimeSDK
installRuntimeSDK:
	flatpak --user install flathub org.freedesktop.Platform/i386/1.6
	flatpak --user install flathub org.freedesktop.Sdk/i386/1.6

#
# -----------------------------------------------------------------------------
# target: buildFlatpak - Build the Flatpak.
#
.PHONY: buildFlatpak
buildFlatpak:
	flatpak-builder --user --arch=i386 --force-clean --repo=$(localFlatpakRepo) $(localFlatpakDir) org.newerth.savagexr.json freedesktop.Platform/i386/1.6 org.freedesktop.Sdk/i386/1.6

#
# -----------------------------------------------------------------------------
# target: buildBundle - Build the Flatpak Bundle.
#
.PHONY: buildBundle
buildBundle:
	flatpak build-bundle --arch=i386 $(localFlatpakRepo) savagexr-i386-$(versionNumber).flatpak org.newerth.savagexr

#
# -----------------------------------------------------------------------------
# target: install - Install the Savage XR Flatpak locally for user (NOT Super User/root, no sudo needed).
#
.PHONY: install
install:
	flatpak --user remote-add --no-gpg-verify --if-not-exists $(localFlatpakRepo) $(localFlatpakRepo)
	flatpak --user install $(localFlatpakRepo) org.newerth.savagexr/i386
	@echo "NOTE: On some DE (Desktop Environments) the menu item will not appear until you logout and back in again. However if you do not want to currently logout you can start the Savage XR Flatpak from the command prompt with: "
	@echo "		flatpak run org.newerth.savagexr"


#
# -----------------------------------------------------------------------------
# target: uninstall - Uninstall the Savage XR Flatpak locally and remove ALL Savage XR files.
#
.PHONY: uninstall
uninstall:
		flatpak --user uninstall org.newerth.savagexr
		flatpak --user remote-delete $(localFlatpakRepo)
		@if test -d ~/.var/app/org.newerth.savagexr ;			\
		then													\
			rm -rf ~/.var/app/org.newerth.savagexr ;			\
		fi
		@if test -d "$(localFlatpakRepo)" ;						\
		then													\
			rm -rf "$(localFlatpakRepo)" ;						\
		fi
		@if test -d "$(localFlatpakDir)" ;						\
		then													\
			rm -rf "$(localFlatpakDir)" ;						\
		fi
		@if test -d .flatpak-builder ;							\
		then													\
			rm -rf .flatpak-builder ;							\
		fi
