				Newerth's Savage XR "Battle for Newerth"
		Unofficial Flatpak Bootstrap (installer/update/runner).
								By C.I.A.
#
# -----------------------------------------------------------------------------
#
Version:	0.9.0
Date:		2020-06-25

Enhancements:
	* Reverted download of Savage XR installer from Flatpak install stage, so that this Flatpak can be distributed as a bundle (single file). Updated bootstrapsavagexr.sh to reflect this.
	* Remove CPU specific optimisation "-march=native" from cflags & cppflags so that Flatpak Bundle is CPU agnostic.
	* Added Flatpak Bundle of Savage XR for easy installation, file 'savagexr-i386-0.9.0.flatpak' with SHA256 checksum: 3891a96070097d6c64e4375bbc0125316e1a36c09a614fb4cdfb3b9f41d43672.
	* Added Bats-core unit tests for bootstrapsavagexr.sh.

Note:
	* The Flatpak Bundle of Savage XR can now be installed with:
		* sha256sum savagexr-i386-0.9.0.flatpak
			* Examine checksum is correct, then:
		* flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
		* flatpak --user install savagexr-i386-0.9.0.flatpak
		* flatpak run org.newerth.savagexr

#
# -----------------------------------------------------------------------------
#
Version:	0.8.0
Date:		2019-04-15

Bug fixes:
	* BUG-003: When running make the Flatpak runtime Platform and Sdk install path are not working on latest version of Flatpak (1.3.1 & builder 1.0.6), causing the user to be prompted with which version to install (1.6 or 18.08). Choosing either make it fail. The paths were missing a forward slash / in the correct location.
		* https://gitlab.com/sea-eye-aya/unofficial-savage-xr-flatpak/issues/3
#
# -----------------------------------------------------------------------------
#
Version:	0.7.0
Date:		2018-08-30

Enhancements:
	* Tidied up Manifest file.
	* Changed download of Savage XR installer to Flatpak install stage (via extra-data section in Manifest), updated bootstrapsavagexr.sh to reflect this.
	* Removed wget Manifest module as using extra-data instead.
	* .savage directly is now preserved via the --persist switch in  the Manifest.
	* Added appdata.xml which is the basic description displayed on Flathub.org and in the Software Centre.


#
# -----------------------------------------------------------------------------
#
Version:	0.6.0
Date:		2018/8/26

Enhancements:
	* Textual changes to README.md.
	* Removal of un-needed multiarch switch from the Manifest


#
# -----------------------------------------------------------------------------
#
Version:	0.5.0
Date:		2018/8/20

Enhancements:
	* Corrected the Application ID to org.newerth.savagexr (from com.s2games.savagexr).

Note:
  * If you need to uninstall Savage XR **make sure** you are using the Savage XR makefile you used to install Savage XR - or it might not find Savage XR to uninstall as now the Application ID has changed.


#
#
# -----------------------------------------------------------------------------
#
Version:	0.4.0
Date:		2018/8/16

Enhancements:
	* Updated Manifest Modules locations to be a bit more reliable internationally.

Bug Fixes:
	* BUG-001: Close shell window during install causes Savage XR to become uninstallable and unplayable.
		* https://gitlab.com/sea-eye-aya/unofficial-savage-xr-flatpak/issues/1
	* BUG-002: Cancel the Savage XR Graphical Installer whilst its installing causes Savage XR not to install or be playable.
		* https://gitlab.com/sea-eye-aya/unofficial-savage-xr-flatpak/issues/2


#
# -----------------------------------------------------------------------------
#
Version:	0.3.0
Date:		2018/8/13

Enhancements:
	* Thanks to Groentjuh for pointing out that there is a config param to stop the AutoUpdater from automatically start Savage XR. This removed the need for renaming the Savage XR binary before and after AutoUpdates.


#
# -----------------------------------------------------------------------------
#
Version:	0.2.0
Date:		2018/8/8

Enhancements:
	* Simplified build process by wrapping it up as a makefile.

Note: Now use:
	* make				To build the Flatpak
	* make install		To install Savage XR locally (does not require SUDO)
	* make uninstall	To remove Savage XR from your system (does not require SUDO)


#
# -----------------------------------------------------------------------------
#
Version:	0.1.0
Date:		2018/7/15

Initial release.
