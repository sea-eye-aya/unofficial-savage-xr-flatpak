#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'


local sFileToChecksum=
local sSHA256Checksum=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	local sFilename=

	sFileToChecksum=$(mktemp)
	echo "something to checksum" >> "$sFileToChecksum"
	read -r sSHA256Checksum sFilename <<< "$(sha256sum "$sFileToChecksum")"
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sFileToChecksum"
}

#------------------------------------------------------------------------------
#
@test "isChecksumOK - Check missing file fails." {

	run isChecksumOK

	assert_failure
	assert_output "sha256sum: 'standard input': no properly formatted SHA256 checksum lines found"
}

#------------------------------------------------------------------------------
#
@test "isChecksumOK - Check missing checksum fails." {

	run isChecksumOK "$sFileToChecksum"

	assert_failure
	assert_output "sha256sum: 'standard input': no properly formatted SHA256 checksum lines found"
}

#------------------------------------------------------------------------------
#
@test "isChecksumOK - Check incorrect checksum fails." {

	run isChecksumOK "$sFileToChecksum" "1111111111111111111111111111111111111111111111111111111111111111"

	assert_failure
	assert_output "$sFileToChecksum: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match"
}

#------------------------------------------------------------------------------
#
@test "isChecksumOK - Check correct checksum passes." {

	run isChecksumOK "$sFileToChecksum" "$sSHA256Checksum"

	assert_success
	assert_output ""
}
