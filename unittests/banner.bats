#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "banner - Check nothing outputted to console if params are missing." {
	
	run banner

	assert_success
	assert_output ""
}

#------------------------------------------------------------------------------
#
@test "banner - Check messages outputted to console." {
	
	run banner "one" "two" "three"

	assert_success
	assert_output "###############################################################################
#
#	one
#	two
#	three
#
###############################################################################"
}
