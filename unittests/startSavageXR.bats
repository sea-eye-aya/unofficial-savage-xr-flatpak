#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}


#------------------------------------------------------------------------------
#
@test "startSavageXR - Check Savage XR bin location and file name (expected to be in current location)." {

	assert_equal "${SAVAGE_BINARY}" "silverback.bin"
}

#------------------------------------------------------------------------------
#
@test "startSavageXR - Check script exited if Savage bin is not found in current directory." {

	SAVAGE_BINARY="MOCK_Savage_bin"

	assert_equal "${SAVAGE_BINARY}" "MOCK_Savage_bin"

	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isFile


	run startSavageXR


	assert_failure
	assert_output "###############################################################################
#
#	Unable to start Savage XR....
#	Cannot locate 'MOCK_Savage_bin' to run Savage XR.
#
###############################################################################"

	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Savage_bin
$sTimeStamp
exitScript(0): "

}

#------------------------------------------------------------------------------
#
@test "startSavageXR - Check Savage runs but generate exit errors, which is expected." {


	SAVAGE_BINARY="MOCK_Savage_bin"

	assert_equal "${SAVAGE_BINARY}" "MOCK_Savage_bin"

	local sTmpDirLocation=$( mktemp --directory )
	local sPWD=$( pwd )
	cd "${sTmpDirLocation}"
	
	echo "#!/bin/bash
echo 'Savage XR MOCK BIN called'
echo 'capture stderr channel too' >&2
exit 121" > "${sTmpDirLocation}/${SAVAGE_BINARY}"
	chmod +x "${sTmpDirLocation}/${SAVAGE_BINARY}"


	function MOCK_Savage_bin() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f MOCK_Savage_bin


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	function chmod() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f chmod


	run startSavageXR


	cd "$sPWD"
	rm --recursive "$sTmpDirLocation"


	assert_success
	assert_output "###############################################################################
#
#	Starting Savage XR...
#
###############################################################################
Savage XR MOCK BIN called
capture stderr channel too
###############################################################################
#
#	Savage XR crashed for unknown reasons.
#	Error code: 121
#	It may have just crashed whilst existing the program...
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Savage_bin
$sTimeStamp
chmod(2): +x ./MOCK_Savage_bin"

}

#------------------------------------------------------------------------------
#
@test "startSavageXR - Check Savage runs." {


	SAVAGE_BINARY="MOCK_Savage_bin"

	assert_equal "${SAVAGE_BINARY}" "MOCK_Savage_bin"

	local sTmpDirLocation=$( mktemp --directory )
	local sPWD=$( pwd )
	cd "${sTmpDirLocation}"
	
	echo "#!/bin/bash
echo 'Savage XR MOCK BIN called'
echo 'capture stderr channel too' >&2
exit 0" > "${sTmpDirLocation}/${SAVAGE_BINARY}"
	chmod +x "${sTmpDirLocation}/${SAVAGE_BINARY}"


	function MOCK_Savage_bin() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f MOCK_Savage_bin


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	function chmod() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f chmod


	run startSavageXR


	cd "$sPWD"
	rm --recursive "$sTmpDirLocation"


	assert_success
	assert_output "###############################################################################
#
#	Starting Savage XR...
#
###############################################################################
Savage XR MOCK BIN called
capture stderr channel too"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Savage_bin
$sTimeStamp
chmod(2): +x ./MOCK_Savage_bin"

}
