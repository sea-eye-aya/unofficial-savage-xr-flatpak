#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}


#------------------------------------------------------------------------------
#
@test "updateSavageXR - Check XR updater bin location and file name (expected to be in current location and run by proxy from shell script)." {

	assert_equal "${SAVAGE_UPDATER}" "savage.sh"
}

#------------------------------------------------------------------------------
#
@test "updateSavageXR - Check script exited if updater is not found." {

	SAVAGE_UPDATER="MOCK_Updater"

	assert_equal "${SAVAGE_UPDATER}" "MOCK_Updater"

	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isFile


	run updateSavageXR


	assert_failure
	assert_output "###############################################################################
#
#	Unable to Update Savage XR....
#	Cannot locate 'MOCK_Updater' to Update Savage XR.
#
###############################################################################"

	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Updater
$sTimeStamp
exitScript(0): "

}

#------------------------------------------------------------------------------
#
@test "updateSavageXR - Check Updater runs but generate errors, which is expected." {


	SAVAGE_UPDATER="MOCK_Updater"

	assert_equal "${SAVAGE_UPDATER}" "MOCK_Updater"

	local sTmpDirLocation=$( mktemp --directory )
	local sPWD=$( pwd )
	cd "${sTmpDirLocation}"
	
	echo "#!/bin/bash
echo 'Savage XR MOCK updater called'
echo 'capture stderr channel too' >&2
exit 64" > "${sTmpDirLocation}/${SAVAGE_UPDATER}"
	chmod +x "${sTmpDirLocation}/${SAVAGE_UPDATER}"


	function MOCK_Updater() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f MOCK_Updater


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	run updateSavageXR


	cd "$sPWD"
	rm --recursive "$sTmpDirLocation"


	assert_success
	assert_output "###############################################################################
#
#	Starting Savage XR Updater.
#
###############################################################################
Savage XR MOCK updater called
capture stderr channel too
###############################################################################
#
#	Updater crashed for unknown reasons.
#	Error code: 64
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Updater"

}

#------------------------------------------------------------------------------
#
@test "updateSavageXR - Check Updater runs." {


	SAVAGE_UPDATER="MOCK_Updater"

	assert_equal "${SAVAGE_UPDATER}" "MOCK_Updater"

	local sTmpDirLocation=$( mktemp --directory )
	local sPWD=$( pwd )
	cd "${sTmpDirLocation}"
	
	echo "#!/bin/bash
echo 'Savage XR MOCK updater called'
echo 'capture stderr channel too' >&2" > "${sTmpDirLocation}/${SAVAGE_UPDATER}"
	chmod +x "${sTmpDirLocation}/${SAVAGE_UPDATER}"


	function MOCK_Updater() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f MOCK_Updater


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	run updateSavageXR


	cd "$sPWD"
	rm --recursive "$sTmpDirLocation"


	assert_success
	assert_output "###############################################################################
#
#	Starting Savage XR Updater.
#
###############################################################################
Savage XR MOCK updater called
capture stderr channel too"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): MOCK_Updater"

}

