#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "exitScript - Check keypress (spacebar) exits script." {

	run exitScript <<< " "

	assert_failure
	assert_output "Press any key to exit..."
}


#------------------------------------------------------------------------------
#
@test "exitScript - Check different keypress (h) exits script." {

	run exitScript <<< "h"

	assert_failure
	assert_output "Press any key to exit..."
}
