#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "isDir - Check a file is not a directory" {

	local sTmpFileLocation=$( mktemp )
	
#echo "# testdata: '$sTmpFileLocation'" >&3

	if isDir "$sTmpFileLocation"; then
		assert_equal "$sTmpFileLocation" "Files are not dirs and should fail"
	else
		assert_equal "$sTmpFileLocation" "$sTmpFileLocation"
	fi
	
	rm "$sTmpFileLocation"
}

#------------------------------------------------------------------------------
#
@test "isDir - Check a directory is a directory" {

	local sTmpDirLocation=$( mktemp --directory )
	
#echo "# testdata: '$sTmpDirLocation'" >&3

	if isDir "$sTmpDirLocation"; then
		assert_equal "$sTmpDirLocation" "$sTmpDirLocation"
	else
		assert_equal "$sTmpDirLocation" "dirs should not fail"
	fi
	
	rm --recursive "$sTmpDirLocation"
}
