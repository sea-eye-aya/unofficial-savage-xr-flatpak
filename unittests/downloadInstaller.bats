#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}

#------------------------------------------------------------------------------
#
@test "downloadInstaller - Check script exits if wget fails to download savage xr installer." {

	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "exitScript(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function wget() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "wget(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f wget


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "rm(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	function chmod() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "chmod(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f chmod


	run downloadInstaller

	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm

	assert_failure
	assert_output "###############################################################################
#
#	Downloading the Savage XR installer, please wait....
#
###############################################################################
###############################################################################
#
#	An ERROR occurred whilst fetching the installer.
#	Removing remains of installer file (if it was downloaded at all)....
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
wget(3): --tries=3 --output-document=${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin http://www.newerth.com/?id=downloads&op=downloadFile&file=xr_setup-1.0-cl_lin_prod.bin&mirrorid=2
$sTimeStamp
rm(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
exitScript(0): "

}

#------------------------------------------------------------------------------
#
@test "downloadInstaller - Check script exits if savage xr installer checksum fails." {

	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function wget() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f wget


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	function chmod() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f chmod


	function isChecksumOK() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isChecksumOK


	run downloadInstaller

	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm

	assert_failure
	assert_output "###############################################################################
#
#	Downloading the Savage XR installer, please wait....
#
###############################################################################
###############################################################################
#
#	An ERROR occurred whilst fetching the installer.
#	SHA256 Checksum mismatch, can occur if download got corrupted
#	Removing remains of installer file....
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
wget(3): --tries=3 --output-document=${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin http://www.newerth.com/?id=downloads&op=downloadFile&file=xr_setup-1.0-cl_lin_prod.bin&mirrorid=2
$sTimeStamp
isChecksumOK(2): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin f9ee596b0a02af69bdca1c51b1a2984edd012f34fb217a20378156ad3f55e380
$sTimeStamp
rm(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
exitScript(0): "

}


#------------------------------------------------------------------------------
#
@test "downloadInstaller - Check that file permission on savage xr installer has EXECUTE added once successfully downloaded and checksum is ok." {

	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function wget() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f wget


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	function chmod() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f chmod


	function isChecksumOK() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isChecksumOK


	run downloadInstaller

	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm

	assert_success
	assert_output "###############################################################################
#
#	Downloading the Savage XR installer, please wait....
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
wget(3): --tries=3 --output-document=${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin http://www.newerth.com/?id=downloads&op=downloadFile&file=xr_setup-1.0-cl_lin_prod.bin&mirrorid=2
$sTimeStamp
isChecksumOK(2): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin f9ee596b0a02af69bdca1c51b1a2984edd012f34fb217a20378156ad3f55e380
$sTimeStamp
chmod(2): +x ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin"

}
