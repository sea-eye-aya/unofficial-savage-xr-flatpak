#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "welcome - Check welcome message output to console." {
	
	run welcome

	assert_success
	assert_output "###############################################################################
#
#	Welcome to Newerth's Savage XR 'Battle for Newerth'
#	unofficial Flatpak Bootstrap (installer/update/runner) Version: ${VERSION}.
#	by C.I.A. June 2018.
#	 
#	For the Newerth.com community, which maintain Savage XR.
#	Your doing a fantastic job people! :)
#	Say NO to racism, xenophobia and all types of bigotry..
#
###############################################################################"
}
