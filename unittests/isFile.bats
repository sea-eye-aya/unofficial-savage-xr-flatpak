#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "isFile - Check a file is a file" {

	local sTmpFileLocation=$( mktemp )

#echo "# testdata: '$sTmpFileLocation'" >&3

	if isFile "$sTmpFileLocation"; then
		assert_equal "$sTmpFileLocation" "$sTmpFileLocation"
	else
		assert_equal "$sTmpFileLocation" "File is not a dir and should fail"
	fi

	rm "$sTmpFileLocation"
}

#------------------------------------------------------------------------------
#
@test "isFile - Check a directory is not a file" {

	local sTmpDirLocation=$( mktemp --directory )

#echo "# testdata: '$sTmpDirLocation'" >&3

	if isFile "$sTmpDirLocation"; then
		assert_equal "$sTmpDirLocation" "Dir is not a file and should fail"
	else
		assert_equal "$sTmpDirLocation" "$sTmpDirLocation"
	fi

	rm --recursive "$sTmpDirLocation"
}
