#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}

#------------------------------------------------------------------------------
#
@test "runInstaller - Check XR installer bin location and file name." {

	assert_equal "${INSTALLER}" "${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin"
}

#------------------------------------------------------------------------------
#
@test "runInstaller - Check Installation directory and symbolic link to it is created, followed by calling the XR installer with correct params." {

	INSTALLER="MOCK_Installer"

	assert_equal "${INSTALLER}" "MOCK_Installer"


	function MOCK_Installer() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f MOCK_Installer


	function mkdir() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f mkdir


	function cd() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f cd


	function ln() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f ln


	run runInstaller


	assert_success
	assert_output "###############################################################################
#
#	Preparing for installer, creating symbolic links etc
#
###############################################################################
###############################################################################
#
#	Running Savage XR CLI installer, this may take some time (~5 minutes).
#	Please wait...
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
mkdir(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
cd(1): ${HOME}
$sTimeStamp
ln(4): -s -T ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr savage-xr
$sTimeStamp
MOCK_Installer(4): --mode console --prefix ${HOME}/savage-xr"

}


#------------------------------------------------------------------------------
#
@test "runInstaller - Check if HOME directory is wrong, then exit script." {

	INSTALLER="MOCK_Installer"

	assert_equal "${INSTALLER}" "MOCK_Installer"


	function MOCK_Installer() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f MOCK_Installer


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function mkdir() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f mkdir


	function cd() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f cd


	function ln() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f ln


	run runInstaller


	assert_failure
	assert_output "###############################################################################
#
#	Preparing for installer, creating symbolic links etc
#
###############################################################################
###############################################################################
#
#	Unable to locate directory: '${HOME}'
#
###############################################################################"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
mkdir(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
cd(1): ${HOME}
$sTimeStamp
exitScript(0): "

}

