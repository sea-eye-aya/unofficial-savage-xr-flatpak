#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}

#------------------------------------------------------------------------------
#
@test "cleanLibs - Check that broken libs are removed/cleaned from Savages own 'libs' directory allowing XR to run." {

	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	run cleanLibs

	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm

	assert_success
	assert_output "Checking for bad lib: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1'
Removing: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1'
Checking for bad lib: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6'
Removing: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6'"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1
$sTimeStamp
rm(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6
$sTimeStamp
rm(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6"

}

#------------------------------------------------------------------------------
#
@test "cleanLibs - Check that broken libs have already been removed." {

	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isFile


	run cleanLibs

	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm

	assert_success
	assert_output "Checking for bad lib: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1'
Checking for bad lib: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6'"


	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libz.so.1
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/libs/libstdc++.so.6"

}