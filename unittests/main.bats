#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

local sMockLog=
local sTimeStamp=

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}

	sMockLog=$(mktemp)
	sTimeStamp=$(date +%s)



	function welcome() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f welcome


	function isFile() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isFile


	function isDir() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isDir


	function isChecksumOK() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f isChecksumOK


	function downloadInstaller() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f downloadInstaller



	function runInstaller() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f runInstaller


	function turnOffAutoStart() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f turnOffAutoStart


	function exitScript() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		exit 1;
	}
	export -f exitScript


	function cleanLibs() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f cleanLibs


	function updateSavageXR() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f updateSavageXR


	function startSavageXR() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f startSavageXR
}

#------------------------------------------------------------------------------
#
teardown() {
	rm "$sMockLog"
}

#------------------------------------------------------------------------------
#
@test "main - Check Installer present but corrupt so remove and download again, then run through installer, updater and launch savage" {


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	local -i iErrorCount=0
	function isFile() {
		local aMockErrorCodes=(0 1 1 1 1 1 1);
		local -i iErrorCode=aMockErrorCodes[iErrorCount];
		iErrorCount+=1;
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";	
		((iErrorCode==0));
	}
	export -f isFile


	function isDir() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isDir


	function isChecksumOK() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f isChecksumOK


	function cd() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f cd


	run main



	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm


	assert_success
	assert_output "###############################################################################
#
#	An ERROR occurred, ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin did not match SHA256 checksum.
#	Suspect install script was perviously terminated manually,
#	removing previous remains of installer file and shall download again.
#
###############################################################################"

	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
welcome(0): 
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isChecksumOK(2): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin f9ee596b0a02af69bdca1c51b1a2984edd012f34fb217a20378156ad3f55e380
$sTimeStamp
rm(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
downloadInstaller(0): 
$sTimeStamp
isDir(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
runInstaller(0): 
$sTimeStamp
turnOffAutoStart(0): 
$sTimeStamp
cd(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
cleanLibs(0): 
$sTimeStamp
updateSavageXR(0): 
$sTimeStamp
cleanLibs(0): 
$sTimeStamp
startSavageXR(0): "

}

#------------------------------------------------------------------------------
#
@test "main - Check Installer downloaded, partically ran but not completed (game directory created but AutoUpdater is missing) so run installer again." {


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	local -i iErrorCount=0
	function isFile() {
		local aMockErrorCodes=(0 0 1 1 1 1 1);
		local -i iErrorCode=aMockErrorCodes[iErrorCount];
		iErrorCount+=1;
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";	
		((iErrorCode==0));
	}
	export -f isFile


	function cd() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f cd


	run main



	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm


	assert_success
	assert_output ""

	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
welcome(0): 
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isChecksumOK(2): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin f9ee596b0a02af69bdca1c51b1a2984edd012f34fb217a20378156ad3f55e380
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isDir(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/savage.sh
$sTimeStamp
runInstaller(0): 
$sTimeStamp
turnOffAutoStart(0): 
$sTimeStamp
cd(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
cleanLibs(0): 
$sTimeStamp
updateSavageXR(0): 
$sTimeStamp
cleanLibs(0): 
$sTimeStamp
startSavageXR(0): "

}

#------------------------------------------------------------------------------
#
@test "main - Check that script exits if installer did not create game directory (no game!)." {


	function rm() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 0;
	}
	export -f rm


	local -i iErrorCount=0
	function isFile() {
		local aMockErrorCodes=(0 0 0 1 1 1 1);
		local -i iErrorCode=aMockErrorCodes[iErrorCount];
		iErrorCount+=1;
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";	
		((iErrorCode==0));
	}
	export -f isFile


	function cd() {
		echo "$sTimeStamp" >> "$sMockLog";
		echo "${FUNCNAME}(${#}): ${*}" >> "$sMockLog";
		return 1;
	}
	export -f cd


	run main



	# Remove Mock rm or we can't access mock logs or bats core output :O
	#
	unset rm


	assert_failure
	assert_output "###############################################################################
#
#	Unable to switch to directory: '${HOME}/.var/app/org.newerth.savagexr/data/savage-xr'
#
###############################################################################"

	local sArguments=$(cat "$sMockLog")
	echo "$sArguments"

	assert_equal "$sArguments" "$sTimeStamp
welcome(0): 
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isChecksumOK(2): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin f9ee596b0a02af69bdca1c51b1a2984edd012f34fb217a20378156ad3f55e380
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/xr_setup-1.0-cl_lin_prod.bin
$sTimeStamp
isDir(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
isFile(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/savage.sh
$sTimeStamp
cd(1): ${HOME}/.var/app/org.newerth.savagexr/data/savage-xr
$sTimeStamp
exitScript(0): "

}

