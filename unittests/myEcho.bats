#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}

#------------------------------------------------------------------------------
#
@test "myEcho - Check output to console." {
	
	run myEcho "Multiple lines\nare supposed and\n\ttabs"

	assert_success
	assert_output "Multiple lines
are supposed and
	tabs"
}
