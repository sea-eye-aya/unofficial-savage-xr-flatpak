#!/usr/bin/env bats
load 'bats-libs/bats-support/load'
load 'bats-libs/bats-assert/load'

# Doc link:
#	https://github.com/ztombol/bats-assert

ScriptToTest='./bootstrapsavagexr.sh'

#------------------------------------------------------------------------------
#
setup() {
	source ${ScriptToTest}
}


#------------------------------------------------------------------------------
#
@test "turnOffAutoStart - Check that AutoUpdater config is in expected location." {

	assert_equal "${AUTOUPDATER_CONFIG}" "${HOME}/.var/app/org.newerth.savagexr/data/savage-xr/au.cfg"
}

#------------------------------------------------------------------------------
#
@test "turnOffAutoStart - Check that auto start XR has been turned off." {

	local sAutoUpdaterConfigFileLocation=$( mktemp )
	
	echo 'debugMode "0"
url "http://www.newerth.com/autoupdater"
maxRetries "3"
remoteCheck "1"
startApplication "1"' > "$sAutoUpdaterConfigFileLocation"

	AUTOUPDATER_CONFIG="$sAutoUpdaterConfigFileLocation"

	assert_equal "${AUTOUPDATER_CONFIG}" "$sAutoUpdaterConfigFileLocation"


	run turnOffAutoStart


	assert_success
	assert_output ""

	local sAutoUpdateConfig=$(cat "$sAutoUpdaterConfigFileLocation")
	echo "$sAutoUpdateConfig"

	assert_equal "$sAutoUpdateConfig" 'debugMode "0"
url "http://www.newerth.com/autoupdater"
maxRetries "3"
remoteCheck "1"
startApplication "0"'


	rm "$sAutoUpdaterConfigFileLocation"
}


#------------------------------------------------------------------------------
#
@test "turnOffAutoStart - Check that auto start XR has been turned off with a weird au.cfg file." {

	local sAutoUpdaterConfigFileLocation=$( mktemp )
	
	echo 'debugMode "0"
url "http://www.newerth.com/autoupdater"
startApplication "1"
startApplication "0"
maxRetries "3"
remoteCheck "1"
startApplication "1"
startApplication			"1"' > "$sAutoUpdaterConfigFileLocation"

	AUTOUPDATER_CONFIG="$sAutoUpdaterConfigFileLocation"

	assert_equal "${AUTOUPDATER_CONFIG}" "$sAutoUpdaterConfigFileLocation"


	run turnOffAutoStart


	assert_success
	assert_output ""

	local sAutoUpdateConfig=$(cat "$sAutoUpdaterConfigFileLocation")
	echo "$sAutoUpdateConfig"

	assert_equal "$sAutoUpdateConfig" 'debugMode "0"
url "http://www.newerth.com/autoupdater"
startApplication "0"
startApplication "0"
maxRetries "3"
remoteCheck "1"
startApplication "0"
startApplication "0"'


	rm "$sAutoUpdaterConfigFileLocation"
}

