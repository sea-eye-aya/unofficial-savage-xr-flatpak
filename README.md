The unofficial "Savage XR - Battle for Newerth" Linux Flatpak
=============================================================
By C.I.A. June 2018.

## What is Savage XR?

Savage XR is a free to play multiplayer online game that is a combination of a team based real-time strategy and a first person shoot'em up. Where each team (Beasts or Humans) works against the other to gain map resources (red stone and gold) in order to construct buildings, defences and weapons. With the winner typically destroying the other teams stronghold. A typical game can last from 5 minutes (fire or demo rush) to over an hour (starting to smell like a draw!).

  * [Official Savage XR website](http://savagexr.com/)
  * [Newerth - Savage XR community site which maintain Savage XR](http://www.newerth.com/)

Savage XR is an extended version of S2 Games "Savage - Battle for Newerth".

## Flatpak Bundle

Savage XR has now been successfully made into a Flatpak Bundle making it easier to install.

  - Download the [Savage XR Flatpak Bundle V0.9.0](https://gitlab.com/sea-eye-aya/unofficial-savage-xr-flatpak/-/releases/Version-0-9-0)
  - Make sure the SHA256 checksum matches: 3891a96070097d6c64e4375bbc0125316e1a36c09a614fb4cdfb3b9f41d43672

		sha256sum savagexr-i386-0.9.0.flatpak

  - Install the Flatpak Bundle with:

		flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
		flatpak --user install savagexr-i386-0.9.0.flatpak
	
  * Finally run the installer which takes approximately 5 minutes, with:

		flatpak run org.newerth.savagexr

The installer is run upon first time execution and downloads the Savage XR installer from Newerth.com.

Under your Applications "Game" menu you will find a new icon to launch Savage XR, called "Savage XR - Battle for Newerth", or you can launch from the command line.

### Play the Tutorials!

For first time player to get the most out of Savage XR (it can be a bit overwhelming if you have never played it before) **please, please, please** work your way through the Tutorials for both Beast and Humans (as there are subtle differences).


## Why has it been Flatpak'ed?

Savage XR is a 32-bit application which has become difficult to install on modern 64-bit Linux distributions. Which often puts new players off from trying to install it.

By wrapping Savage XR up in a Flatpak environment it makes it easier to install and maintain when Savage Autoupdates are applied.

(It should be pointed out that Mac OS and Windows versions of Savage XR are also available.)

## What do I need?
  * A modern Linux distribution, either 32-bit or 64-bit, such as Debian 9, Devuan 2, Arch, Ubuntu 18.04, Gentoo, Fedora 28 etc.
  * 3GB of ram (currently [2018] the installer ([InstallJammer](https://en.wikipedia.org/wiki/InstallJammer)) will consume all ram and hang on a 2GB machine due to a bug in the [OSTree library](https://wiki.gnome.org/Projects/OSTree), see below). Although the game itself does not require anywhere near this amount.
  * 3GB of disk space during building and installation, although some of that can be reclaimed.
  * A Hardware Accelerated graphics card, the faster the better but it will work on low end cards such as Intel GM45 or Radeon R300.
  * Hardware accelerated OpenGL drivers.
  * Flatpak v0.11.8 or above.
  * Flatpak-Builder v0.99.1 or above, which probably wont be in your repo..
  * Internet connection, about 1.5GBs of data get downloaded in total.

# Installation Guide

The Flatpak Bundle is all you need to start play Savage XR, but if you'd like to build the Flatpak locally from scratch then this is the guide for you.

You will be building/compiling the Savage XR environment in a Flatpak locally.

**NO S2 Games or Savage XR copyrighted resources/material/products are included in the Flatpak**.

It is only when you run the Flatpak for the first time that Savage XR will be downloaded and installed.


## Step 1 - Installing Flatpak and Flatpak-builder

Hopefully these will be found in your distributions repository, make sure that they have the same or newer versions of Flatpak v0.11.8 or above.

**Especially Flatpak-builder** (the build process wont currently [2018] work with the older v0.8.x series of the builder, due to unsupported commands), unfortunately the newer 0.9.x series probably hasn't trickled down into your distros repo yet.

Follow the [Quick Setup](https://flatpak.org/setup/) on the Flatpak.org website.

Debian 9 - Stretch (and Devuan 2 - ASCII) users can find a newer version of Flatpak-builder in the Debian 9 (Stretch) backport repo:

  * [backports.debian.org](https://backports.debian.org)
  * [List of Stretch backports](https://backports.debian.org/changes/stretch-backports.html)


## Step 2 - Build the Flatpak

This will install the build environment locally in your user account (as opposed to system wide), add the Flathub.org repository to Flatpak and install the Flatpak Freedesktop Runtime Platform and SDK.

The build process will then download and compile the required libraries (such as [SDL](http://libsdl.org/) and [Harfbuzz](https://www.freedesktop.org/wiki/Software/HarfBuzz/)), this takes about +5 minutes on a modern [2018] machine, and place the Flatpak in a local repository on your machine.

The advantage of building the Flatpak locally is that the downloaded libraries can be more aggressively compiled to support special features of your CPU (as opposed to say a generic i386/AMD64 compilation).

### Optional aggressive optimization

>You can tune the Flatpak to your CPU by modifying the build options in the `org.newerth.savagexr.json` file to:

		"build-options": {
			"cflags": "-march=native -O3 -pipe",
			"cppflags": "-march=native -O3 -pipe"
		},


From the projects directory (containing the makefile) type:

		make

This process may produce warning, these can be ignored, if any errors occur then Flatpak stops the build process.


## Step 3 - Optionally build the Flatpak Bundle

As you are building the Flatpak locally there is no need to make the Flatpak Bundle and this step can be skipped, unless you want to install your Flatpak on more than one machine. Also should be noted that if you change the build flags to be CPU specific with `-march=native` the Bundle will only work on PC's with the same model of CPU.

From the projects directory (containing the makefile) type:

		make buildFlatpak

This will place the `savagexr-i386-0.9.0.flatpak` Bundle file in the current directory.


## Step 4 - Install the created Savage XR Flatpak

Install the Savage XR Flatpak with (does NOT require SUDO):

		make install

This will add an icon "Savage XR - Battle for Newerth" to your Applications menu, in the sub entry called "Game".

NOTE: On some DE (Desktop Environments) the menu item will not appear until you logout and back in again. However if you do not want to currently logout you can start the Savage XR Flatpak from the command prompt with:

		flatpak run org.newerth.savagexr


## Step 5 - Starting Savage XR for the first time (installing the game)

Under your Applications "Game" menu you will find a new icon to launch Savage XR, called "Savage XR - Battle for Newerth". 

Clicking on this icon will open a terminal window that will install Savage XR for you.

Once the Savage XR installer has been downloaded it will automatically start, follow the instructions in the terminal window:

		Running Savage XR installer, please choose default location.
		But DE-SELECT:
			* Launch Savage XR (as this flatpak installer needs to run further)
			* Create menu Shortcuts (not needed)
			* Create Desktop Shortcut (not needed)
		Then click on the 'Finish' button.

The Savage Autoupdater will now launch and update Savage XR to the latest version, when it has completed Savage XR will launch.

## Step 6 - Success!

You have installed Savage XR and it is ready to play!

Choose a login name and password, don't forget to write them down!

## Step 7 - What next? Play the Tutorials!

For first time player to get the most out of Savage XR (it can be a bit overwhelming if you have never played it before) **please, please, please** work your way through the Tutorials for both Beast and Humans (as there are subtle differences).

Once you've finished the tutorials and are now trying to find a game server to connection to, its best to order the list by number of players and then choose the server with the most players with the lowest ping as the combat is in real-time so the lower the delay between you and the server the better.

# Starting Savage XR

Once fully installed you can start Savage XR from your **Application menu**, in the sub entry called **Game** by clicking on the icon **Savage XR - Battle for Newerth**.

## Alternative starting methods.

### From the command prompt
You can start Savage XR from the command line with:

		flatpak run org.newerth.savagexr

(This is the actual command used to launch Savage XR from your Applications menu.)

### Starting from inside the Flatpak

This method starts Savage XR from INSIDE the Flatpak, for debugging purposes mainly, first start a console/shell inside the Savage XR Flatpak with:

		flatpak run --devel --command=sh org.newerth.savagexr

You can then start Savage XR with the command:

		bootstrapsavagexr.sh

Which is the custom launch script.

Once finished type:

		exit

From the command prompt will exit you from the Savage XR Flatpak.

# Uninstalling Savage XR Flatpak

To remove Savage XR (does NOT require SUDO), from the projects directory (containing the makefile) type:

		make uninstall

If the icons have not been removed from your Application menu you can manually remove them from:

		~/.local/share/applications

# Known bugs/Issues

  * Exiting Savage XR causes a Seg Fault, obviously not freeing up memory upon exit but does not cause an issue (other than not being able to restart Savage XR from within its Options menu).
  * Savage XR wont install on a 2GB machine or less is due to the [OSTree library](https://wiki.gnome.org/Projects/OSTree), the installer ([InstallJammer](https://en.wikipedia.org/wiki/InstallJammer)) starts using massive amounts of ram but does not use any swap ram so runs out of ram. This does not occur when installing natively:
	  * [flatpak build-bundle causes glib memory error when operating on v. large bundles #483](https://github.com/flatpak/flatpak/issues/483)
	  * [Use streaming reads for delta superblock for better memory use with inline parts #1551](https://github.com/ostreedev/ostree/pull/1551)
	
	Once this gets fixed and the changes trickle down to your distributions repositories Savage XR will be installable on a 2GB or less machine. Although there is a work around (see below).
  * Tearing on nVidia cards, a noticeable line is present 1/3 of the way down the screen when v-sync is switched on. Enabling your desktop Compositor (such as Compton or Marco) removes the line.
  * Upon installation the "Game" menu item and "Savage XR - Battle for Newerth" icon do not appear. On some DE (Desktop Environments) the menu item will not appear until you logout and back in again. However if you do not want to currently logout you can start the Savage XR Flatpak from the command prompt.
  * No audio when running for the first time. This seems to be a permissions issue between Flatpak and user space PulseAudio mixer (PulseAudio has had permission issues when running on a multi-user system in the past). Logout and back in again has cured this issue in some circumstances, or it may be that PulseAudio just needs restarting (depends on distro but usually "pulseaudio -k" to kill and "pulseaudio --start" to restart). Failing that and your on a multi-user system make sure your user has permissions to access PulseAudio, check that PulseAudio is defaulting to the correct Profile in the "PulseAudio Volume Control" if you have multiple sound cards (chances are your GFX card has audio outputs as well).
  * Savage XR has stopped working giving a libGL error messages:

	\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#  
	\#  
	\#	Starting Savage XR...  
	\#  
	\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#  
	System_Init()  
	libGL error: No matching fbConfigs or visuals found  
	libGL error: failed to load driver: swrast  
	\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#  
	\#  
	\#	Savage XR crashed for unknown reasons.  
	\#	Error code: 1  
	\#	It may have just crashed whilst existing the program...  
	\#  
	\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#  

	This is due to Flatpak's GFX drivers dropping out of sync with the drivers on your system, for example NVIDIA Driver Version: 390.48 are currently installed but Flatpak was installed against NVIDIA Driver Version: 384.130 and so its API is pointing to a version no longer on the system (Flatpak is expecting 384.130). **Updating Flatpak will resolve this issue**, type:
	  * flatpak update

	Flatpak will install the required driver. If your using bleeding edge drivers it may be that Flatpak have not packaged them yet, remember major and minor version numbers need to match.


# Work around for 2GB or less machines

This will only work if you have access to a existing Savage XR Linux installation.

By copying the "savage-xr" directory into the Flatpak the Installer does not need to run and thus avoid the out of memory issue.

The Flatpak directory can be accessed from your home directory (via a hidden folder):

		~/.var/app/org.newerth.savagexr/data

And will need to contain BOTH the Savage XR installer (xr_setup-1.0-cl_lin_prod.bin) and the savage-xr directory containing all the Savage XR files and data (such as silverback.bin). The Savage XR installer (xr_setup-1.0-cl_lin_prod.bin) needs to be present or the bootstrapsavagexr.sh script will try to download it and re-install Savage XR..

Launching Savage XR will now pick up the existing directory and you can play Savage XR on a PC with less that 2GB of ram :)

(Or wait until the OSTree library bug gets fixed.)


# Performance impact of running Savage XR under Flatpak

So far the only difference seems to be 5-10% difference in frame rate, so if your within 5-10% of your desired v-sync frame rate (e.g. only getting 54-57fps when you want 60fps) it might be worth trying to install Savage XR natively.


# Native installation of Savage XR

For those that want to try Zorn and DogTheBountyHunter of the Newerth community have written a installation guide for native Savage XR installation under a modern 64-bit Linux distro here:

  * [How to Install Savage XR on 64-bit Ubuntu 14.04](http://www.newerth.com/smf/index.php/topic,17598.0.html)

Don't forget to remove **~/savage-xr/libs/libz.so.1** each time after SavageAutoupdater has done an update! (grrrrr!)


# Turning V-sync off/on

Currently [2018] if you have an AMD/ATI/Radeon or Intel graphics chip there is no way to turn vertical synchronisation on or off from the Display Preferences. nVidia owners can achieve this via their NVIDIA X Server Settings (nvidia-settings) under OpenGL Settings tab, setting "Sync to VBlank".

You have to use the command prompt or modify the Application menus launch icon:

		flatpak run org.newerth.savagexr
Becomes

		vblank_mode=0 flatpak run org.newerth.savagexr

Or to force v-sync on:

		flatpak run org.newerth.savagexr
Becomes

		vblank_mode=1 flatpak run org.newerth.savagexr

Further details can be found here:

  * [Disable vertical sync for glxgears](https://stackoverflow.com/questions/17196117/disable-vertical-sync-for-glxgears)
  * [Arch Linux wiki ATI Turn vsync off](https://wiki.archlinux.org/index.php/ATI#Turn_vsync_off)
  * [Arch Linux wiki Intel Disable Vertical Synchronization (VSYNC) ](https://wiki.archlinux.org/index.php/Intel#Disable_Vertical_Synchronization_.28VSYNC.29)

For Intel GPUs if you turn off v-sync you may benefit from turning off tripple buffering as well (making Savage XR more responsive, especially at low frame rates).

Turning off (or on) your desktop Compositor (such as Compton or Marco) may also yield faster frame rates and have an effect on v-sync.


# Development notes

## Bash script development.

Code style has been to follow the **Defensive BASH Programming** guide by Kfir Lavi:

  * [Defensive BASH Programming](https://kfirlavi.herokuapp.com/blog/2012/11/14/defensive-bash-programming/)
  * [Useful Bash reference](https://devhints.io/bash)

### Shell Check

Use **shellcheck** (it'll be in your Distro repo) to Lint/error check the bash scripts (bootstrapsavagexr.sh), fix any errors that it finds.

Why? Because Bash scripts are complex and obscure errors can hide anywhere!


### Unit testing with Bats-core.

Bash Automated Testing System - Bats.

The original Bats project stagnated and got forked into Bats-core.

**Bats-core 1.2.0-dev** has been used to run the test suite.

Unit tests are written using **bats-assert** to improve readability and are located in the `'unofficial-savage-xr-flatpak/unittests'` directory, each test file (ending in .bats) is a bash script testing a separate function in the **bootstrapsavagexr.sh** script.

At the time of writing **bats-core** is in a slight state of flux, in that they are updating it with the inclusion of additional 3rd party helper libraries/functions but have not finished these changes yet.

So the following additional libraries are needed to be manually placed in the `'unofficial-savage-xr-flatpak/unittests/bats-libs'` directory:

  * [bats-support - Latest commit 004e707 Dec 6, 2016](https://github.com/ztombol/bats-support)
  * [bats-assert  - Latest commit 9f88b42 Mar 22, 2016](https://github.com/ztombol/bats-assert)  

Either install bats-core from your Distro repo if it is new enough, it may be named just '**bats**'. Or install from:
  * [bats-core - Latest commit 87b16eb Oct 7, 2019](https://github.com/bats-core/bats-core)

Test suite is run from `'unofficial-savage-xr-flatpak'` with:

	$ bats unittests/

Or individual tests with:

	$ bats unittests/invalidPackageName.bats

Syntax hi-lighting can be enabled by associating **.bats** in your editor (such as Geany or Eclipse) with the Shell Editor (Bash).
